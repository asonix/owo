public struct Streamdeck.ReadInput {
    public uint8 key;
    public string serial_number;
}

public struct Streamdeck.DeckInfo {
    public string serial_number;
    public string device_name;
    public string port_name;
}

public struct Streamdeck.CommandInfo {
    public uint8 key;
    public string command;
}

public struct InputName {
    public uint8 key;
    public string name;
}

public class Streamdeck.Daemon : Object {
    [DBus (name = "dog.asonix.git.asonix.Streamdeck")]
    private interface StreamdeckBackend : Object {
        public async abstract string[] get_scenes () throws GLib.Error;
        public async abstract void enable_discovery () throws GLib.Error;
        public async abstract void disable_discovery () throws GLib.Error;
        public async abstract DeckInfo[] get_decks () throws GLib.Error;
        public async abstract string connect (string host, uint16 port) throws GLib.Error;
        public async abstract string disconnect () throws GLib.Error;
        public async abstract string get_state () throws GLib.Error;
        public async abstract string login (string password) throws GLib.Error;
        public async abstract CommandInfo[] get_commands (string serial_number) throws GLib.Error;
        public async abstract ReadInput[] read_input () throws GLib.Error;
        public async abstract void set_input (string serial_number, uint8 key, string command) throws GLib.Error;
        public async abstract void unset_input (string serial_number, uint8 key) throws GLib.Error;
        public async abstract void set_input_name (string serial_number, uint8 key, string name) throws GLib.Error;
        public async abstract InputName[] get_input_names (string serial_number) throws GLib.Error;
        public async abstract void set_deck_name (string serial_number, string name) throws GLib.Error;
        public async abstract string get_deck_name (string serial_number) throws GLib.Error;
    }

    private static Daemon? _instance;

    public static Daemon instance {
        get {
            if (_instance == null) {
                _instance = new Daemon ();
            }

            return _instance;
        }
    }

    private StreamdeckBackend? backend_object;
    private string? obs_state;
    private DeckInfo[] decks;
    private string[] scenes;
    private Gee.HashMap<string, Gee.ArrayList<CommandInfo?>> commands = new Gee.HashMap<string, Gee.ArrayList<CommandInfo?>> ();
    private Gee.HashMap<string, Gee.HashMap<uint8, string>> input_names = new Gee.HashMap<string, Gee.HashMap<uint8, string>> ();
    private Gee.HashMap<string, string> deck_names = new Gee.HashMap<string, string> ();

    public signal void dbus_connection_signal ();
    public signal void obs_state_signal (string state);
    public signal void decks_signal (DeckInfo[] decks);
    public signal void on_decks_added (DeckInfo[] decks);
    public signal void on_decks_removed (DeckInfo[] decks);
    public signal void scenes_signal (string[] scenes);
    public signal void commands_signal (string serial_number, Gee.ArrayList<CommandInfo?> commands);
    public signal void key_press_signal (ReadInput key_press);
    public signal void input_name_signal (string serial_number, uint8 key, string name);
    public signal void deck_name_signal (string serial_number, string name);

    construct {
        dbus_connection_signal.connect ((_obj) => {
            do_load_state ();
            do_load_decks ();

            GLib.Timeout.add_seconds_full (GLib.Priority.DEFAULT, 3, () => {
                do_load_state ();
                do_load_decks ();

                return true;
            });
        });

        obs_state_signal.connect ((_obj, state) => {
            on_state_change.begin (state, (_obj, res) => {
                try {
                    on_state_change.end (res);
                } catch (Error e) {
                    print ("State handler error: %s\n", e.message);
                }
            });
        });

        decks_signal.connect ((_obj, deck_list) => {
            var new_decks = diff_decks (decks, deck_list);
            var removed_decks = diff_decks (deck_list, decks);

            decks = deck_list;

            on_decks_added (new_decks);
            on_decks_removed (removed_decks);

            foreach (DeckInfo deck_info in new_decks) {
                update_command_cache (deck_info.serial_number);
                update_name_cache (deck_info.serial_number);
            }
        });

        scenes_signal.connect ((_obj, new_scenes) => {
            scenes = new_scenes;
        });

        commands_signal.connect ((_obj, serial_number, new_commands) => {
            if (commands.has_key (serial_number)) {
                commands.unset (serial_number);
            }
            commands.set (serial_number, new_commands);
        });

        Bus.get_proxy.begin<StreamdeckBackend> (
            BusType.SESSION,
            "dog.asonix.git.asonix.Streamdeck",
            "/dog/asonix/git/asonix/Streamdeck",
            0,
            null,
            (_obj, res) => {
                try {
                    backend_object = Bus.get_proxy.end (res);
                    dbus_connection_signal ();
                } catch (Error e) {
                    error ("Streamdeck error: %s", e.message);
                }
            }
        );
    }

    public string get_obs_state () {
        if (obs_state == null) {
            return "Disconnected";
        } else {
            return obs_state;
        }
    }

    public DeckInfo[] get_decks () {
        return decks;
    }

    public string[] get_scenes () {
        return scenes;
    }

    public Gee.ArrayList<CommandInfo?> get_commands (string serial_number) {
        return commands.get (serial_number);
    }

    public string? get_deck_name (string serial_number) {
        return deck_names.get (serial_number);
    }

    public string? get_input_name (string serial_number, uint8 key) {
        var hm = input_names.get (serial_number);
        if (hm != null) {
            return hm.get (key);
        }
        return null;
    }

    public async void set_deck_name (string serial_number, string name) throws GLib.Error {
        disconnected_err ();

        yield backend_object.set_deck_name (serial_number, name);
    }

    public async void set_input_name (string serial_number, uint8 key, string name) throws GLib.Error {
        disconnected_err ();

        yield backend_object.set_input_name (serial_number, key, name);
    }

    public async void add_command (string serial_number, uint8 key, string command) throws GLib.Error {
        disconnected_err ();

        yield backend_object.set_input (serial_number, key, command);
    }

    public async void remove_command (string serial_number, uint8 key) throws GLib.Error {
        disconnected_err ();

        yield backend_object.unset_input (serial_number, key);
    }

    public async void connect_obs () throws GLib.Error {
        disconnected_err ();

        string host;
        uint16 port;
        App.obs_settings.get ("host", "s", out host);
        App.obs_settings.get ("port", "q", out port);

        var state = yield backend_object.connect (host, port);
        obs_state_signal (state);
    }

    public async void authenticate_obs (string passw0rt) throws GLib.Error {
        disconnected_err ();

        var state = yield backend_object.login (passw0rt);
        obs_state_signal (state);
    }

    public async void disconnect_obs () throws GLib.Error {
        disconnected_err ();

        var state = yield backend_object.disconnect ();
        obs_state_signal (state);
    }

    public async void key_press () throws GLib.Error {
        disconnected_err ();

        var read_inputs = yield backend_object.read_input ();
        foreach (ReadInput input in read_inputs) {
            key_press_signal (input);
        }
    }

    public void update_command_cache (string serial_number) {
        load_commands.begin (serial_number, (_obj, res) => {
            try {
                var new_commands = load_commands.end (res);
                var array_list = new Gee.ArrayList<CommandInfo?> ((a, b) => {
                    return a.key == b.key && a.command == b.command;
                });

                foreach (CommandInfo info in new_commands) {
                    array_list.add (info);
                }

                commands_signal (serial_number, array_list);
            } catch (Error e) {
                print ("Command fetch error: %s\n", e.message);
            }
        });
    }

    public void update_name_cache (string serial_number) {
        load_deck_name.begin (serial_number, (_obj, res) => {
            try {
                var deck_name = load_deck_name.end (res);
                deck_names.set(serial_number, deck_name);
                deck_name_signal (serial_number, deck_name);
            } catch {
                // do nothing, this is fine
            }
        });

        load_input_names.begin (serial_number, (_obj, res) => {
            try {
                var names = load_input_names.end (res);
                foreach (InputName name in names) {
                    if (!input_names.has_key (serial_number)) {
                        input_names.set (serial_number, new Gee.HashMap<uint8, string> ());
                    }
                    var hm = input_names.get (serial_number);
                    if (hm.has_key (name.key)) {
                        hm.unset (name.key);
                    }
                    hm.set (name.key, name.name);
                    input_name_signal (serial_number, name.key, name.name);
                }
            } catch (Error e) {
                print ("Name fetch error: %s\n", e.message);
            }
        });
    }

    private async string[] load_scenes () throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_scenes ();
    }

    private void do_load_decks () {
        load_decks.begin ((_obj, res) => {
            try {
                var new_decks = load_decks.end (res);
                decks_signal (new_decks);
            } catch (Error e) {
                print ("Get decks error: %s\n", e.message);
            }
        });
    }

    private async DeckInfo[] load_decks () throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_decks ();
    }

    private async CommandInfo[] load_commands (string serial_number) throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_commands (serial_number);
    }

    private async string load_deck_name (string serial_number) throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_deck_name (serial_number);
    }

    private async InputName[] load_input_names (string serial_number) throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_input_names (serial_number);
    }

    private async void on_state_change (string state) throws GLib.Error {
        if (obs_state == state) {
            return;
        }

        obs_state = state;

        if (state == "Disconnected") {
            yield connect_obs ();
        } else if (state == "Connected") {
            var new_scenes = yield load_scenes ();
            scenes_signal (new_scenes);
        }
    }

    private void do_load_state () {
        load_state.begin ((_obj, res) => {
            try {
                var state = load_state.end (res);
                obs_state_signal (state);
            } catch (Error e) {
                print ("Get state error: %s\n", e.message);
            }
        });
    }

    private async string load_state () throws GLib.Error {
        disconnected_err ();

        return yield backend_object.get_state ();
    }

    private void disconnected_err () throws GLib.Error {
        if (backend_object == null) {
            throw new GLib.IOError.FAILED ("Not connected to streamdeck daemon");
        }
    }

    private DeckInfo[] diff_decks(DeckInfo[] lhs, DeckInfo[] rhs) {
        DeckInfo[] new_decks = new DeckInfo[0];

        var exists = false;
        foreach (DeckInfo deck_info in rhs) {
            exists = false;
            foreach (DeckInfo existing_info in lhs) {
                exists = exists
                    || deck_info.serial_number == existing_info.serial_number;
            }
            if (!exists) {
                new_decks += deck_info;
            }
        }

        return new_decks;
    }
}
