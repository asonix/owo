namespace Streamdeck.Views {
    public class DeckStack : Gtk.Stack {
        construct {
            add_named (new Widgets.EmptyConfigPane (), "empty-state");

            show_all();

            Daemon.instance.key_press_signal.connect (handle_key_press);
            Daemon.instance.commands_signal.connect (handle_commands);
            Daemon.instance.input_name_signal.connect (handle_input_name_change);
        }

        public void select_deck (string serial_number) {
            var child = get_child_by_name (serial_number);
            if (child != null) {
                set_visible_child_name (serial_number);
            }
        }

        public void add_deck (DeckInfo deck_info) {
            if (get_child_by_name (deck_info.serial_number) != null) {
                return;
            }

            var pane = new Widgets.CommandList (deck_info);
            add_named (pane, deck_info.serial_number);
        }

        public void remove_deck (DeckInfo deck_info) {
            var child = get_child_by_name (deck_info.serial_number);
            if (child != null) {
                remove (child);
            }
        }

        private void handle_key_press (ReadInput key_info) {
            var child = get_visible_child ();
            if (child == null) {
                return;
            }

            var list = (Widgets.CommandList) child;
            list.key_press (key_info);
        }

        private void handle_input_name_change (string serial_number, uint8 key, string name) {
            var child = get_child_by_name (serial_number);
            if (child == null) {
                return;
            }

            var list = (Widgets.CommandList) child;
            list.input_name_change (key, name);
        }

        private void handle_commands (string serial_number, Gee.ArrayList<CommandInfo?> commands) {
            var child = get_child_by_name (serial_number);
            if (child == null) {
                return;
            }

            var list = (Widgets.CommandList) child;
            list.command_list (commands);
        }
    }
}
