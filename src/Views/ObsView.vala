namespace Streamdeck.Views {
    public class ObsView : Gtk.Grid {
        private Gtk.Button connect_button;
        private Gtk.Revealer auth_revealer;
        private Gtk.Entry auth_entry;

        construct {
            column_spacing = 12;
            row_spacing = 12;
            halign = Gtk.Align.CENTER;
            margin_top = 64;

            var host_label = new Gtk.Label (_("Hostname:"));
            var host_entry = new Gtk.Entry ();
            host_entry.valign = Gtk.Align.CENTER;
            host_entry.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);

            var port_label = new Gtk.Label (_("Port:"));
            var port_entry = new Gtk.SpinButton.with_range (1, 65535, 1);
            port_entry.valign = Gtk.Align.CENTER;
            port_entry.snap_to_ticks = true;

            App.obs_settings.bind (
                "host",
                host_entry,
                "text",
                GLib.SettingsBindFlags.DEFAULT
            );

            App.obs_settings.bind (
                "port",
                port_entry,
                "value",
                GLib.SettingsBindFlags.DEFAULT
            );

            connect_button = new Gtk.Button ();

            var auth_label = new Gtk.Label (_("Password:"));
            auth_entry = new Gtk.Entry ();
            auth_entry.valign = Gtk.Align.CENTER;
            auth_entry.set_visibility (false);
            auth_entry.set_input_purpose (Gtk.InputPurpose.PASSWORD);
            auth_entry.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);

            var auth_grid = new Gtk.Grid ();
            auth_grid.column_spacing = 12;
            auth_grid.row_spacing = 12;
            auth_grid.attach (auth_label, 0, 2);
            auth_grid.attach (auth_entry, 1, 2);

            auth_revealer = new Gtk.Revealer ();
            auth_revealer.child = auth_grid;
            auth_revealer.reveal_child = false;

            attach (host_label, 0, 0);
            attach (host_entry, 1, 0);
            attach (port_label, 0, 1);
            attach (port_entry, 1, 1);
            attach (auth_revealer, 0, 2, 2);
            attach (connect_button, 1, 3);

            show_all ();

            Daemon.instance.obs_state_signal.connect (handle_state_change);
            connect_button.clicked.connect (handle_button_click);

            switch (Daemon.instance.get_obs_state ()) {
                case "Disconnected":
                    connect_button.label = _("Connect");
                    break;
                case "Unauthenticated":
                    connect_button.label = _("Login");
                    auth_revealer.reveal_child = true;
                    break;
                default:
                    connect_button.label = _("Reconnect");
                    break;
            }
        }

        private void handle_state_change (string state) {
            if (!connect_button.sensitive) {
                connect_button.sensitive = true;
            }

            switch (state) {
                case "Disconnected":
                    connect_button.label = _("Connect");
                    break;
                case "Unauthenticated":
                    auth_revealer.reveal_child = true;
                    break;
                default:
                    connect_button.label = _("Reconnect");
                    break;
            }
        }

        private void handle_button_click () {
            connect_button.sensitive = false;
            switch (Daemon.instance.get_obs_state ()) {
                case "Disconnected":
                    Daemon.instance.connect_obs.begin ();
                    break;
                case "Unauthenticated":
                    Daemon.instance.authenticate_obs.begin (auth_entry.text);
                    break;
                default:
                    Daemon.instance.disconnect_obs.begin ();
                    break;
            }
        }
    }
}
