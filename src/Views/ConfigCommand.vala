namespace Streamdeck.Views {
    public class ConfigCommand : Gtk.Grid {
        private Gtk.Stack command_stack;
        private Widgets.ScenesComboBox scenes_combobox;
        private Widgets.CommandComboBox command_combobox;
        private string serial_number;
        private string? command_name;
        private uint8? key;
        private Data.Command? command;
        private Gtk.Entry name_entry;

        public signal void changed (Data.Command command);

        public ConfigCommand (string serial_number) {
            this.serial_number = serial_number;

            build ();

            command_combobox.select_first ();
            scenes_combobox.select_first ();
        }

        public ConfigCommand.from_existing (string serial_number, string? name, Data.Command command) {
            this.serial_number = serial_number;
            this.key = command.get_command_key ();
            if (name == null) {
                this.command_name = "%d".printf (this.key);
            } else {
                this.command_name = name;
            }
            this.command = command;

            build ();

            switch (command.get_command_type ()) {
                case Data.CommandType.SWITCH_SCENE:
                    var scene_name = ((Data.SwitchScene) command).scene_name;
                    scenes_combobox.set_active_id (scene_name);
                    command_combobox.set_active_id ("SwitchScene");
                    break;
                default:
                    break;
            }
        }

        private void build () {
            var command_title = new Gtk.Label (
                _("Configure the command you wish send")
            );
            command_title.hexpand = true;
            command_title.get_style_context ().add_class (Granite.STYLE_CLASS_H2_LABEL);
            command_title.max_width_chars = 75;
            command_title.wrap = true;
            command_title.wrap_mode = Pango.WrapMode.WORD_CHAR;
            command_title.xalign = 0;
            command_title.margin_bottom = 24;

            var command_label = new Gtk.Label (
                _("Command:")
            );
            command_label.halign = Gtk.Align.START;
            command_label.valign = Gtk.Align.START;

            command_combobox = new Widgets.CommandComboBox ();

            var scenes_label = new Gtk.Label (_("to"));
            scenes_combobox = new Widgets.ScenesComboBox ();

            var switch_scene_grid = new Gtk.Grid ();
            switch_scene_grid.attach (scenes_label, 0, 0);
            switch_scene_grid.attach (scenes_combobox, 1, 0, 2);
            switch_scene_grid.column_spacing = 12;
            switch_scene_grid.row_spacing = 12;
            switch_scene_grid.halign = Gtk.Align.CENTER;

            command_stack = new Gtk.Stack ();
            command_stack.add_named (new Gtk.Label ("..."), "Empty");
            command_stack.add_named (switch_scene_grid, "SwitchScene");

            var name_label = new Gtk.Label (_("Name:"));
            name_entry = new Gtk.Entry ();
            name_entry.valign = Gtk.Align.CENTER;
            name_entry.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);
            if (command_name != null) {
                name_entry.text = command_name;
            } else if (key != null) {
                command_name = "%d".printf (key);
                name_entry.text = command_name;
            }

            column_spacing = 12;
            row_spacing = 6;
            halign = Gtk.Align.CENTER;
            valign = Gtk.Align.CENTER;
            vexpand = true;
            margin = 24;

            attach (command_title, 0, 0, 3);
            attach (name_label, 0, 1);
            attach (name_entry, 1, 1, 2);
            attach (command_label, 0, 2);
            attach (command_combobox, 1, 2);
            attach (command_stack, 2, 2);

            show_all ();

            changed.connect (handle_change);
            name_entry.changed.connect (handle_name_change);
            scenes_combobox.selected.connect (handle_scene_change);
            command_combobox.selected.connect (handle_type_change);
        }

        public void set_key (uint8 key) {
            this.key = key;
            if (command_name == null) {
                command_name = "%d".printf (key);
            }

            var type = command_combobox.get_selected_type ();

            if (type != null) {
                handle_type_change (type);
            }
        }

        private void handle_change (Data.Command cmd) {
            Daemon.instance.add_command.begin (
                serial_number,
                cmd.get_command_key (),
                cmd.to_json (),
                (obj, res) => {
                    try {
                        Daemon.instance.add_command.end (res);
                        Daemon.instance.update_command_cache (serial_number);
                    } catch (Error e) {
                        print ("Error saving command %s\n", e.message);
                    }
                }
            );
        }

        private void handle_name_change () {
            if (command_name != name_entry.text && key != null) {
                command_name = name_entry.text;
                Daemon.instance.set_input_name.begin (
                    serial_number,
                    key,
                    name_entry.text,
                    (_obj, res) => {
                        try {
                            Daemon.instance.set_input_name.end (res);
                            Daemon.instance.update_name_cache (serial_number);
                        } catch {
                            // do nothing
                        }
                    }
                );
            }
        }

        private void handle_type_change (Data.CommandType type) {
            switch (type) {
                case Data.CommandType.SWITCH_SCENE:
                    command_stack.set_visible_child_name ("SwitchScene");
                    var scene_name = scenes_combobox.get_selected_scene ();
                    if (scene_name != null) {
                        handle_scene_change (scene_name);
                    }
                    break;
                default:
                    break;
            }
        }

        private void handle_scene_change (string scene_name) {
            if (command != null && command.is_type (Data.CommandType.SWITCH_SCENE)) {
                var switch_scene = (Data.SwitchScene) command;
                switch_scene.scene_name = scene_name;
                changed (command);
            } else if (key != null) {
                command = new Data.SwitchScene (key, scene_name);
                changed (command);
            }
        }
    }
}
