namespace Streamdeck.Views {
    public class DeckView : Gtk.Grid {
        construct {
            column_homogeneous = true;
            column_spacing = 12;
            row_spacing = 12;

            DeckStack deck_stack = new Views.DeckStack ();
            deck_stack.margin = 12;
            deck_stack.margin_start = 0;

            var deck_list = new Widgets.DeckList (deck_stack);

            foreach (DeckInfo deck_info in Daemon.instance.get_decks()) {
                deck_list.add_deck (deck_info);
            }

            if (!deck_list.any_selected ()) {
                deck_list.select_first_item ();
            }

            deck_list.show_all ();
            deck_stack.show_all ();

            var scrolled_window = new Gtk.ScrolledWindow (null, null);
            scrolled_window.add (deck_list);
            scrolled_window.expand = true;

            var sidebar = new Gtk.Grid ();
            sidebar.orientation = Gtk.Orientation.VERTICAL;
            sidebar.add(scrolled_window);
            sidebar.margin = 12; 
            sidebar.margin_end = 0;

            attach (sidebar, 0, 0);
            attach (deck_stack, 1, 0, 2, 1);

            Daemon.instance.on_decks_added.connect ((_obj, decks) => {
                foreach (DeckInfo deck_info in decks) {
                    deck_list.add_deck (deck_info);
                }

                if (!deck_list.any_selected ()) {
                    deck_list.select_first_item ();
                }
            });

            Daemon.instance.on_decks_removed.connect ((_obj, decks) => {
                foreach (DeckInfo deck_info in decks) {
                    deck_list.remove_deck (deck_info);
                }

                if (!deck_list.any_selected ()) {
                    deck_list.select_first_item ();
                }

                deck_list.show_all ();
            });

            show_all ();
        }
    }

}
