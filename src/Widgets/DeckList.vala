namespace Streamdeck.Widgets {
    public class DeckList : Gtk.Frame {
        private Gtk.Label usb_l;
        private Views.DeckStack deck_stack;
        private Gtk.ListBox list_box;

        public DeckList (Views.DeckStack deck_stack) {
            this.deck_stack = deck_stack;
        }

        construct {
            var alert = new Granite.Widgets.AlertView (
                _("No streamdecks found"),
                _("Try plugging one in!"),
                ""
            );
            alert.show_all();

            list_box = new Gtk.ListBox ();
            list_box.selection_mode = Gtk.SelectionMode.SINGLE;
            list_box.activate_on_single_click = true;
            list_box.set_placeholder (alert);

            usb_l = new Gtk.Label (_("Usb"));
            usb_l.get_style_context ().add_class (Granite.STYLE_CLASS_H4_LABEL);
            usb_l.halign = Gtk.Align.START;

            list_box.set_header_func (update_headers);

            var scroll = new Gtk.ScrolledWindow (null, null);
            scroll.hscrollbar_policy = Gtk.PolicyType.NEVER;
            scroll.expand = true;
            scroll.add (list_box);

            add (scroll);

            show_all ();

            list_box.row_selected.connect ((row) => {
                if (row == null) {
                    return;
                }

                row.activate ();
                var item = (DeckItem) row;
                deck_stack.select_deck (item.serial_number);
            });

            Daemon.instance.deck_name_signal.connect (handle_deck_name);
        }

        public void add_deck (DeckInfo deck_info) {
            var already_present = false;

            var name = Daemon.instance.get_deck_name (deck_info.serial_number);
            if (name != null) {
                deck_info.device_name = name;
            }

            foreach (Gtk.Widget child in list_box.get_children ()) {
                if (child == null) {
                    continue;
                }

                var deck_item = (DeckItem) child;

                already_present = already_present
                    || deck_item.serial_number == deck_info.serial_number;
            }

            if (!already_present) {
                var item = new DeckItem (
                    deck_info.serial_number,
                    deck_info.device_name,
                    deck_info.port_name
                );

                deck_stack.add_deck (deck_info);
                list_box.add (item);
            }

            deck_stack.show_all ();
            show_all ();
        }

        public void remove_deck (DeckInfo deck_info) {
            foreach (Gtk.Widget child in list_box.get_children ()) {
                if (child == null) {
                    continue;
                }

                var deck_item = (DeckItem) child;

                if (deck_item.serial_number == deck_info.serial_number) {
                    deck_stack.remove_deck (deck_info);
                    list_box.remove (deck_item);
                    break;
                }
            }

            deck_stack.show_all ();
            show_all ();
        }

        private void handle_deck_name (string serial_number, string name) {
            foreach (Gtk.Widget row in list_box.get_children ()) {
                if (row == null) {
                    continue;
                }

                var item = (DeckItem) row;
                if (item.serial_number == serial_number) {
                    item.device_name = name;
                }
            }
        }

        private void update_headers (Gtk.ListBoxRow row, Gtk.ListBoxRow? before = null) {
            if (before != null) {
                row.set_header (null);
                return;
            }

            if (usb_l.get_parent () != null) {
                usb_l.unparent ();
            }

            row.set_header (usb_l);
        }

        public bool any_selected () {
            return list_box.get_selected_row () != null;
        }

        public void select_first_item () {
            var row = list_box.get_row_at_index (0);
            if (row == null) {
                return;
            }
            list_box.select_row (row);
        }
    }
}
