namespace Streamdeck.Widgets {
    public class DisconnectedPage : Gtk.Grid {
        construct {
            var title_label = new Gtk.Label (_("OBS is currently disconnected"));
            title_label.hexpand = true;
            title_label.get_style_context ().add_class (Granite.STYLE_CLASS_H2_LABEL);
            title_label.max_width_chars = 75;
            title_label.wrap = true;
            title_label.wrap_mode = Pango.WrapMode.WORD_CHAR;
            title_label.xalign = 0;

            var description_label = new Gtk.Label (
                _("Try connecting it before configuring commands")
            );
            description_label.hexpand = true;
            description_label.max_width_chars = 75;
            description_label.wrap = true;
            description_label.use_markup = true;
            description_label.xalign = 0;
            description_label.valign = Gtk.Align.START;

            var layout = new Gtk.Grid ();
            layout.column_spacing = 12;
            layout.row_spacing = 6;
            layout.halign = Gtk.Align.CENTER;
            layout.valign = Gtk.Align.CENTER;
            layout.vexpand = true;
            layout.margin = 24;

            layout.attach (title_label, 1, 1);
            layout.attach (description_label, 1, 2);

            add (layout);

            show_all ();
        }
    }
}
