namespace Streamdeck.Widgets {
    public class EmptyConfigPane: Gtk.Grid {
        construct {
            column_spacing = 12;
            row_spacing = 12;
            halign = Gtk.Align.CENTER;
            margin = 24;
            margin_top = 64;

            var label = new Gtk.Label (
                _("No streamdeck selected")
            );
            label.valign = Gtk.Align.CENTER;
            label.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);

            attach (label, 0, 0);

            show_all ();
        }
    }
}
