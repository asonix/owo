namespace Streamdeck.Widgets {
    public class CommandList : Gtk.Frame {
        private string serial_number;
        private Gtk.ListBox list_box;
        private Dialogs.NewCommandDialog? new_command_dialog;
        private Gee.HashMap<uint8, CommandRow> row_map = new Gee.HashMap<uint8, CommandRow> ();

        public signal void key_press (ReadInput key_info);
        public signal void input_name_change (uint8 key, string name);
        public signal void command_list (Gee.ArrayList<CommandInfo?> commands);

        public CommandList(DeckInfo info) {
            this.serial_number = info.serial_number;

            var alert = new Granite.Widgets.AlertView (
                _("No commands registered"),
                _("Try adding a new command."),
                ""
            );
            alert.show_all ();

            list_box = new Gtk.ListBox ();
            list_box.selection_mode = Gtk.SelectionMode.SINGLE;
            list_box.activate_on_single_click = true;
            list_box.set_placeholder (alert);

            var add_button = new Gtk.Button.from_icon_name (
                "list-add-symbolic",
                Gtk.IconSize.BUTTON
            );
            add_button.tooltip_text = _("Add");

            var remove_button = new Gtk.Button.from_icon_name (
                "list-remove-symbolic",
                Gtk.IconSize.BUTTON
            );
            remove_button.tooltip_text = _("Remove");

            var actionbar = new Gtk.ActionBar ();
            actionbar.get_style_context ().add_class (Gtk.STYLE_CLASS_INLINE_TOOLBAR);
            actionbar.add (add_button);
            actionbar.add (remove_button);
            actionbar.show_all ();

            var scrolled = new Gtk.ScrolledWindow (null, null);
            scrolled.expand = true;
            scrolled.add (list_box);

            key_press.connect ((_obj, key_info) => {
                if (new_command_dialog != null) {
                    new_command_dialog.key_press (key_info);
                }
            });

            var commands = Daemon.instance.get_commands (serial_number);
            if (commands != null) {
                handle_command_list (commands);
            }

            var grid = new Gtk.Grid ();

            grid.attach (scrolled, 0, 0);
            grid.attach (actionbar, 0, 1);

            add (grid);

            remove_button.clicked.connect (handle_remove_click);
            add_button.clicked.connect (handle_add_click);
            command_list.connect (handle_command_list);
            input_name_change.connect (handle_input_name_change);

            show_all ();
        }

        private void handle_remove_click () {
            var row = list_box.get_selected_row ();
            if (row == null) {
                return;
            }

            var command_row = (CommandRow) row;
            var key = command_row.get_key ();

            Daemon.instance.remove_command.begin (serial_number, key, (_obj, res) => {
                try {
                    Daemon.instance.remove_command.end (res);
                    Daemon.instance.update_command_cache (serial_number);

                    print ("Widgets.CommandList unset");
                    row_map.unset (key);
                    print ("Widgets.CommandList after unset");
                    list_box.remove (row);
                } catch (Error e) {
                    print ("Error removing command: %s\n", e.message);
                }
            });
        }

        private void handle_add_click () {
            if (new_command_dialog == null) {
                new_command_dialog = new Dialogs.NewCommandDialog (serial_number);
                new_command_dialog.transient_for = (Gtk.Window) get_toplevel ();
                new_command_dialog.show_all ();

                new_command_dialog.response.connect ((response_id) => {
                    if (response_id == Gtk.ResponseType.REJECT) {
                        var app = (MainWindow) get_toplevel ();
                        app.to_obs ();
                    }

                    new_command_dialog.destroy ();
                });

                new_command_dialog.destroy.connect (() => {
                    new_command_dialog = null;
                });
            }

            new_command_dialog.present ();
        }

        private void handle_command_list (Gee.ArrayList<CommandInfo?> commands) {
            foreach (CommandInfo cmd_info in commands) {
                var command = Data.Command.parse (cmd_info.key, cmd_info.command);

                if (command != null) {
                    var existing = row_map.get (cmd_info.key);
                    if (existing != null) {
                        list_box.remove (existing);
                        print ("Widgets.CommandList unset");
                        row_map.unset (cmd_info.key);
                        print ("Widgets.CommandList after unset");
                    }

                    var name = Daemon.instance.get_input_name (serial_number, cmd_info.key);

                    var row = new CommandRow (serial_number, name, command);
                    row_map.set (cmd_info.key, row);
                    list_box.add (row);
                }
            }
        }

        private void handle_input_name_change (uint8 key, string name) {
            var row = row_map.get (key);
            if (row == null) {
                return;
            }
            var command_row = (CommandRow) row;
            command_row.name_change (name);
        }
    }
}
