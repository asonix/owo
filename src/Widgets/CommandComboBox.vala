namespace Streamdeck.Widgets {
    public class CommandComboBox : Gtk.ComboBoxText {
        private Data.CommandType? _selected;

        public signal void selected (Data.CommandType type);

        construct {
            id_column = 1;

            foreach (Data.CommandDescription cmd in Data.Command.available ()) {
                append (cmd.id, cmd.name);
            }

            changed.connect ((_obj) => {
                var id = get_active_id ();
                var type = Data.Command.type_string (id);

                if (type != null) {
                    selected (type);
                }

                _selected = type;
            });

            show_all ();
        }

        public Data.CommandType? get_selected_type () {
            return _selected;
        }

        public void select_first () {
            var id = get_active_id ();
            if (id != null) {
                return;
            }

            var available = Data.Command.available ();
            if (available.length == 0) {
                return;
            }

            set_active_id (available[0].id);
        }
    }
}
