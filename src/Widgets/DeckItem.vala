namespace Streamdeck.Widgets {
    public class DeckItem : Gtk.ListBoxRow {
        public string serial_number { get; set; }
        public string device_name { get; set; }
        public string port_name { get; set; }
        private Dialogs.EditDeckDialog? edit_deck_dialog;

        public DeckItem (string serial_number, string device_name, string port_name) {
            Object (
                serial_number: serial_number,
                device_name: device_name,
                port_name: port_name
           );
        }

        construct {
            var row_title = new Gtk.Label (device_name);
            row_title.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);
            row_title.halign = Gtk.Align.START;
            row_title.valign = Gtk.Align.START;
            row_title.ellipsize = Pango.EllipsizeMode.END;
            row_title.show_all ();

            var row_description = new Gtk.Label (port_name);
            row_description.margin_top = 2;
            row_description.use_markup = true;
            row_description.halign = Gtk.Align.START;
            row_description.valign = Gtk.Align.START;
            row_description.ellipsize = Pango.EllipsizeMode.END;
            row_description.show_all ();

            var edit_button = new Gtk.Button.from_icon_name (
                "document-edit",
                Gtk.IconSize.BUTTON
            );
            edit_button.tooltip_text = _("Rename");
            edit_button.halign = Gtk.Align.END;
            edit_button.valign = Gtk.Align.CENTER;
            edit_button.expand = true;

            var row_grid = new Gtk.Grid ();
            row_grid.margin = 6;
            row_grid.margin_start = 3;
            row_grid.column_spacing = 3;
            row_grid.attach (row_title, 0, 0, 2);
            row_grid.attach (row_description, 0, 1, 2);
            row_grid.attach (edit_button, 1, 0, 1, 2);
            row_grid.show_all ();

            add (row_grid);

            bind_property ("device-name", row_title, "label");
            bind_property ("port-name", row_description, "label");

            show_all ();

            edit_button.clicked.connect (handle_edit_click);
        }

        private void handle_edit_click () {
            if (edit_deck_dialog == null) {
                edit_deck_dialog = new Dialogs.EditDeckDialog (
                    serial_number,
                    device_name
                );
                edit_deck_dialog.transient_for = (Gtk.Window) get_toplevel ();
                edit_deck_dialog.show_all ();

                edit_deck_dialog.response.connect ((_response_id) => {
                    edit_deck_dialog.destroy ();
                });

                edit_deck_dialog.destroy.connect (() => {
                    edit_deck_dialog = null;
                });
            }

            edit_deck_dialog.present ();
        }
    }
}
