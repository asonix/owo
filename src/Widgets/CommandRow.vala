namespace Streamdeck.Widgets {
    public class CommandRow : Gtk.ListBoxRow {
        private string serial_number;
        private string? command_name;
        private Data.Command command;
        private Dialogs.EditCommandDialog? edit_dialog;
        private Gtk.Label row_key;

        public signal void name_change (string name);

        public CommandRow (string serial_number, string? command_name, Data.Command command) {
            this.serial_number = serial_number;
            this.command_name = command_name;
            this.command = command;

            var key = command.get_command_key ();
            row_key = new Gtk.Label ("%d".printf (key));
            if (command_name != null) {
                row_key.label = command_name;
            }
            row_key.halign = Gtk.Align.START;
            row_key.valign = Gtk.Align.CENTER;
            row_key.show_all ();

            var row_command = new Gtk.Label (null);
            row_command.halign = Gtk.Align.START;
            row_command.valign = Gtk.Align.CENTER;
            row_command.show_all ();

            var inner_grid = new Gtk.Grid ();
            inner_grid.halign = Gtk.Align.START;
            inner_grid.valign = Gtk.Align.CENTER;
            inner_grid.orientation = Gtk.Orientation.HORIZONTAL;
            inner_grid.margin = 6;
            inner_grid.margin_start = 3;
            inner_grid.column_spacing = 3;

            inner_grid.add (row_command);

            switch (command.get_command_type ()) {
                case Data.CommandType.SWITCH_SCENE:
                    row_command.label = _("Switch scene");
                    switch_scene_ui (inner_grid);
                    break;
                default:
                    row_command.label = _("Unknown Command");
                    break;
            }

            var row_edit = new Gtk.Button.from_icon_name (
                "document-edit",
                Gtk.IconSize.BUTTON
            );
            row_edit.tooltip_text = _("Edit");
            row_edit.halign = Gtk.Align.END;
            row_edit.valign = Gtk.Align.CENTER;
            row_edit.expand = true;

            var row_grid = new Gtk.Grid ();
            row_grid.orientation = Gtk.Orientation.HORIZONTAL;
            row_grid.margin = 6;
            row_grid.margin_start = 3;
            row_grid.column_spacing = 12;
            row_grid.add (row_key);
            row_grid.add (inner_grid);


            row_grid.add (row_edit);

            row_grid.show_all();

            add (row_grid);

            show_all ();

            row_edit.clicked.connect (handle_edit_click);
            name_change.connect (handle_name_change);

        }

        public uint8 get_key () {
            return command.get_command_key ();
        }

        private void handle_edit_click () {
            if (edit_dialog == null) {
                edit_dialog = new Dialogs.EditCommandDialog (serial_number, command_name, command);
                edit_dialog.transient_for = (Gtk.Window) get_toplevel ();
                edit_dialog.show_all ();

                edit_dialog.destroy.connect (() => {
                    edit_dialog = null;
                });
            }

            edit_dialog.present ();
        }

        private void handle_name_change (string name) {
            command_name = name;
            row_key.label = name;
        }

        private void switch_scene_ui (Gtk.Grid row_grid) {
            var switch_scene = (Data.SwitchScene) command;
            var to = new Gtk.Label (_("to"));
            to.halign = Gtk.Align.START;
            to.valign = Gtk.Align.CENTER;
            to.show_all ();

            var scene_name = new Gtk.Label (switch_scene.scene_name);
            scene_name.halign = Gtk.Align.START;
            scene_name.valign = Gtk.Align.CENTER;
            scene_name.show_all ();

            row_grid.add (to);
            row_grid.add (scene_name);
        }
    }
}
