namespace Streamdeck.Widgets {
    public class ScenesComboBox : Gtk.ComboBoxText {
        public string? _selected;
        public signal void selected (string scene_name);

        construct {
            id_column = 1;

            foreach (var scene in Daemon.instance.get_scenes ()) {
                append (scene, scene);
            }

            show_all ();

            changed.connect (() => {
                var scene_name = get_active_text ();

                if (scene_name != null) {
                    selected (scene_name);
                }

                _selected = scene_name;
            });
        }

        public string? get_selected_scene () {
            return _selected;
        }

        public void select_first () {
            var scene_name = get_active_text ();
            if (scene_name != null) {
                return;
            }

            var scenes = Daemon.instance.get_scenes ();
            if (scenes.length == 0) {
                return;
            }

            set_active_id (scenes[0]);
        }
    }
}
