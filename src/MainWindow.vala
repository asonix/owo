public class Streamdeck.MainWindow : Hdy.ApplicationWindow {
    public const string ACTION_PREFIX = "win.";
    public const string ACTION_QUIT = "action_quit";

    public Views.DeckStack deck_stack { get; private set; }
    public Gtk.Stack main_stack { get; private set; }

    private uint configure_id;

    private const ActionEntry[] ACTION_ENTRIES = {
        { ACTION_QUIT, action_quit }
    };

    public MainWindow (Gtk.Application application) {
        Object (application: application);

        application.set_accels_for_action (
            ACTION_PREFIX + ACTION_QUIT,
            {"<Control>q", "<Control>w"}
        );
    }

    construct {
        Hdy.init ();

        add_action_entries (ACTION_ENTRIES, this);
    }

    public void build_ui () {
        height_request = 350;
        width_request = 400;
        title = _("Streamdeck");

        int window_x, window_y, window_width, window_height;
        App.saved_state.get ("window-position", "(ii)", out window_x, out window_y);
        App.saved_state.get ("window-size", "(ii)", out window_width, out window_height);

        set_default_size (window_width, window_height);

        if (window_x != -1 || window_y != -1) {
            move (window_x, window_y);
        }

        if (App.saved_state.get_boolean ("window-maximized")) {
            maximize ();
        }

        main_stack = new Gtk.Stack ();
        main_stack.add_titled (new Views.DeckView (), "deck-config", _("Streamdecks"));
        main_stack.add_titled (new Views.ObsView (), "obs-config", _("OBS"));

        var stack_switcher = new Gtk.StackSwitcher ();
        stack_switcher.margin = 12;
        stack_switcher.halign = Gtk.Align.CENTER;
        stack_switcher.homogeneous = true;
        stack_switcher.stack = main_stack;

        var headerbar = new Hdy.HeaderBar ();
        headerbar.show_close_button = true;
        headerbar.set_title (_("Streamdeck"));
        headerbar.show_all ();

        var grid = new Gtk.Grid ();
        grid.attach (headerbar, 0, 0);
        grid.attach (stack_switcher, 0, 1);
        grid.attach (main_stack, 0, 2);
        grid.show_all ();

        add (grid);
    }

    private void action_quit () {
        destroy ();
    }

    public void to_obs () {
        main_stack.set_visible_child_name ("obs-config");
    }

    public override bool configure_event (Gdk.EventConfigure event) {
        if (configure_id == 0) {
            configure_id = Timeout.add (200, () => {
                configure_id = 0;

                App.saved_state.set_boolean ("window-maximized", is_maximized);

                if (!is_maximized) {
                    int width, height, root_x, root_y;
                    get_position (out root_x, out root_y);
                    get_size (out width, out height);

                    App.saved_state.set ("window-position", "(ii)", root_x, root_y);
                    App.saved_state.set ("window-size", "(ii)", width, height);
                }

                return GLib.Source.REMOVE;
            });
        }

        return base.configure_event (event);
    }
}
