namespace Streamdeck.Data {
    public class SwitchScene : Command {
        public string scene_name;

        public SwitchScene (uint8 key, string scene_name) {
            base (key, CommandType.SWITCH_SCENE);
            this.scene_name = scene_name;
        }

        public override string to_json () {
            var gen = new Json.Generator ();
            var root = new Json.Node (Json.NodeType.OBJECT);
            var object = new Json.Object ();
            root.set_object (object);
            gen.set_root (root);

            object.set_string_member ("type", "SwitchScene");
            object.set_string_member ("name", scene_name);

            size_t length;
            string json = gen.to_data (out length);

            return json;
        }
    }
}
