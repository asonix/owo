namespace Streamdeck.Data {
    public enum CommandType {
        SWITCH_SCENE,
    }

    public struct CommandDescription {
        CommandType type;
        string name;
        string id;
    }

    public abstract class Command : GLib.Object {
        private CommandType type;
        private uint8 key;

        protected Command (uint8 key, CommandType type) {
            this.key = key;
            this.type = type;
        }

        public static CommandDescription[] available() {
            CommandDescription[] available = {
                CommandDescription() {
                    type = CommandType.SWITCH_SCENE,
                    name = _("Switch Scene"),
                    id = "SwitchScene"
                }
            };

            return available;
        }

        public static CommandType? type_string (string? type) {
            switch (type) {
                case "SwitchScene":
                    return CommandType.SWITCH_SCENE;
                default:
                    return null;
            }
        }

        public static Command? parse (uint8 key, string command) {
            var parser = new Json.Parser ();

            try {
                parser.load_from_data (command);
                var obj = parser.get_root ().get_object ();

                switch (type_string(obj.get_string_member ("type"))) {
                    case CommandType.SWITCH_SCENE:
                        var scene_name = obj.get_string_member ("name");
                        return new SwitchScene (key, scene_name);
                    default:
                        return null;
                }
            } catch {
                return null;
            }
        }

        public abstract string to_json ();

        public bool is_type (CommandType rhs) {
            return type == rhs;
        }

        public CommandType get_command_type () {
            return type;
        }

        public uint8 get_command_key () {
            return key;
        }
    }
}
