public class Streamdeck.App : Gtk.Application {
    public static GLib.Settings obs_settings { get; private set; }
    public static GLib.Settings saved_state { get; private set; }
    public static MainWindow main_window { get; private set; }

    static construct {
        obs_settings = new GLib.Settings ("dog.asonix.git.asonix.owo.obs");
        saved_state = new GLib.Settings ("dog.asonix.git.asonix.owo.saved-state");
    }

    construct {
        flags = ApplicationFlags.FLAGS_NONE;
        application_id = "dog.asonix.git.asonix.owo";

        var present_action = new SimpleAction ("app.present", null);
        present_action.activate.connect (() => {
            if (main_window != null) {
                main_window.present_with_time ((uint32) GLib.get_monotonic_time ());
            }
        });

        add_action (present_action);
    }

    protected override void activate () {
        upgrade_elementary_style ();

        if (main_window == null) {
            main_window = new MainWindow (this);
            main_window.build_ui ();
            add_window (main_window);
        }

        main_window.present ();
    }

    private void upgrade_elementary_style () {
        const string STYLESHEET_PREFIX = "io.elementary.stylesheet";
        unowned var gtk_settings = Gtk.Settings.get_default ();

        if (gtk_settings.gtk_theme_name == "elementary") {
            gtk_settings.gtk_cursor_theme_name = "elementary";
            gtk_settings.gtk_icon_theme_name = "elementary";

            gtk_settings.gtk_theme_name = string.join (".", STYLESHEET_PREFIX, "grape"); // i like purple
        }
    }

    public static int main (string[] args) {
        return new Streamdeck.App ().run (args);
    }
}
