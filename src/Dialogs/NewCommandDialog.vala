namespace Streamdeck.Dialogs {
    public class NewCommandDialog : Granite.Dialog {
        private Gtk.Stack stack;
        private Views.ConfigCommand command_page;
        private string serial_number;
        private ReadInput key_info;

        public signal void key_press (ReadInput key_info);

        public NewCommandDialog (string serial_number) {
            this.serial_number = serial_number;

            var disconnected_page = new Widgets.DisconnectedPage ();

            var press_label = new Gtk.Label (
                _("Please press the button you wish to program on your stream deck")
            );
            press_label.hexpand = true;
            press_label.get_style_context ().add_class (Granite.STYLE_CLASS_H2_LABEL);
            press_label.max_width_chars = 30;
            press_label.wrap = true;
            press_label.wrap_mode = Pango.WrapMode.WORD_CHAR;
            press_label.xalign = 0;

            var press_page = new Gtk.Grid ();
            press_page.column_spacing = 12;
            press_page.row_spacing = 12;
            press_page.halign = Gtk.Align.CENTER;
            press_page.margin = 24;

            press_page.attach (press_label, 0, 0);

            var existing_key_label = new Gtk.Label (
                _("Key already mapped, try pressing a new key")
            );
            existing_key_label.hexpand = true;
            existing_key_label.get_style_context ().add_class (Granite.STYLE_CLASS_H2_LABEL);
            existing_key_label.max_width_chars = 30;
            existing_key_label.wrap = true;
            existing_key_label.wrap_mode = Pango.WrapMode.WORD_CHAR;
            existing_key_label.xalign = 0;

            var existing_key_page = new Gtk.Grid ();
            existing_key_page.column_spacing = 12;
            existing_key_page.row_spacing = 12;
            existing_key_page.halign = Gtk.Align.CENTER;
            existing_key_page.margin = 24;

            existing_key_page.attach (existing_key_label, 0, 0);

            command_page = new Views.ConfigCommand (serial_number);

            stack = new Gtk.Stack ();
            stack.add_named (disconnected_page, "disconnected");
            stack.add_named (press_page, "keypress");
            stack.add_named (existing_key_page, "existing");
            stack.add_named (command_page, "command");

            get_content_area ().add (stack);

            if (Daemon.instance.get_obs_state () != "Connected") {
                add_button ("Close", Gtk.ResponseType.REJECT);
            } else {
                add_button ("Close", Gtk.ResponseType.CLOSE);
            }

            show_all ();

            key_press.connect (handle_key_press);

            if (Daemon.instance.get_obs_state () != "Connected") {
                stack.set_visible_child_name ("disconnected");
            } else {
                stack.set_visible_child_name ("keypress");
                Daemon.instance.key_press.begin ();
            }
        }

        private void handle_key_press (ReadInput key_info) {
            if (serial_number == key_info.serial_number) {
                if (already_pressed (key_info.key)) {
                    stack.set_visible_child_name ("existing");
                    Daemon.instance.key_press.begin ();
                } else {
                    this.key_info = key_info;
                    command_page.set_key (key_info.key);
                    stack.set_visible_child_name ("command");
                }
            } else {
                Daemon.instance.key_press.begin ();
            }
        }

        private bool already_pressed (uint8 key) {
            foreach (CommandInfo cmd in Daemon.instance.get_commands (serial_number)) {
                if (cmd.key == key) {
                    return true;
                }
            }

            return false;
        }
    }
}
