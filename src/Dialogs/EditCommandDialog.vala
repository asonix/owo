namespace Streamdeck.Dialogs {
    public class EditCommandDialog : Granite.Dialog {
        public EditCommandDialog (string serial_number, string? command_name, Data.Command initial_command) {
            var disconnected_page = new Widgets.DisconnectedPage ();

            var command_page = new Views.ConfigCommand.from_existing (serial_number, command_name, initial_command);

            var stack = new Gtk.Stack ();
            stack.add_named (disconnected_page, "disconnected");
            stack.add_named (command_page, "command");

            get_content_area ().add (stack);

            if (Daemon.instance.get_obs_state () != "Connected") {
                add_button ("Close", Gtk.ResponseType.REJECT);
            } else {
                add_button ("Close", Gtk.ResponseType.CLOSE);
            }

            show_all ();

            if (Daemon.instance.get_obs_state () != "Connected") {
                stack.set_visible_child_name ("disconnected");
            } else {
                stack.set_visible_child_name ("command");
            }

            response.connect ((response_id) => {
                if (response_id == Gtk.ResponseType.REJECT) {
                    var app = (MainWindow) transient_for;
                    app.to_obs ();
                }

                destroy ();
            });
        }
    }
}
