namespace Streamdeck.Dialogs {
    public class EditDeckDialog : Granite.Dialog {
        private string previous_name;
        private string serial_number;
        private Gtk.Entry entry;

        public EditDeckDialog (string serial_number, string deck_name) {
            this.serial_number = serial_number;
            this.previous_name = deck_name;

            var label = new Gtk.Label (_("Edit Deck Name"));
            entry = new Gtk.Entry ();
            entry.valign = Gtk.Align.CENTER;
            entry.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);
            entry.text = deck_name;

            var grid = new Gtk.Grid ();
            grid.column_spacing = 12;
            grid.row_spacing = 12;
            grid.halign = Gtk.Align.CENTER;
            grid.margin = 24;

            grid.attach (label, 0, 0);
            grid.attach (entry, 1, 0, 3);

            get_content_area ().add (grid);

            entry.changed.connect (handle_entry_change);

            add_button ("Close", Gtk.ResponseType.CLOSE);

            show_all ();
        }

        private void handle_entry_change () {
            if (previous_name != entry.text) {
                previous_name = entry.text;
                Daemon.instance.set_deck_name.begin (serial_number, entry.text, (_obj, res) => {
                    try {
                        Daemon.instance.set_deck_name.end (res);
                        Daemon.instance.update_name_cache (serial_number);
                    } catch {
                        // do nothing
                    }
                });
            }
        }
    }
}
