#!/usr/bin/env bash

set -xe

flatpak-builder build dog.asonix.git.asonix.owo.yml --user --install --force-clean
flatpak run dog.asonix.git.asonix.owo
